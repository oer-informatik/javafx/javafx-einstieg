## Aufbau eines ersten "Hello World" (Scenegraph)


<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/jfx_03_aufbau_des_scenegraphs</span>


> **tl/dr;** _(ca. 4 min Lesezeit): Eine JavaFX-App ist als Scenegraph aufgebaut, der sich in die Komponenten Stage, Scene und (Root/Branch/Leaf)-Nodes gliedert. Diese Bestandteile und deren Aufbau über das Composite-Pattern werden erläutert. Der Artikel ist Teil eines [JavaFX-Tutorials](https://oer-informatik.de/javafx-projekt)._

### Bühne frei: die Komponenten einer JavaFX-GUI

JavaFX gliedert den Aufbau einer GUI in begrifflicher Anlehnung an ein Theater. Wir gehen die Komponenten anhand folgender Zeichnung durch:

![Der Aufbau einer JavaFX-GUI](images/Aufbau/00_StageSceneGraph.png)

Der Ort, an dem die GUI präsentiert wird, ist die **Stage**, die Bühne des Theaters. Gemeint ist damit das Fenster (bei Desktop-Programmen), dass sich öffnet, wenn ich das Programm starte (oder der gesamte Programmbildschirm bei Tablet/Smartphone-Anwendungen). Die Erzeugung der _Stage_ wird vom Framework übernommen (siehe Sequenzdiagramm: `LauncherImpl` erzeugt sie). Wir erhalten die _Stage_ als Parameter unserer `start()`-Methode:  

```java
@Override
public void start(Stage primaryStage){
    //...
    primaryStage.setTitle("Hello World!");
    primaryStage.setScene(scene);
    primaryStage.show();
}
```

Auf dieser Bühne (_Stage_) wird eine Szene (**Scene**) abgespielt. Eine Szene hat eine bestimmte Größe und enthält den eigentlichen Aufbau. Es können bei Bedarf auch mehrere Szenen vorbereitet werden, die nacheinander auf die Bühne geholt werden.

```java
    //...
    Scene scene = new Scene(rootNode, 300, 250);
    //... 
    primaryStage.setScene(scene);
```

Der Aufbau einer Szene besteht aus mehreren verschachtelten Elementen. Das erste Element, dass alle weiteren enthält, nennt man **root node**, den Wurzelknoten. Ein Wurzelknoten kann aus weiteren strukturierenden Verzweigungsknoten (**branch node**) bestehen (z.B. Listen, Tabellen, Stapeln), die ihrerseits weitere Knoten enthalten.

Die eigentlichen Inhalte (z.B. Texte, Bilder, Knöpfe, Grafiken) stellen Blätter dar (**leaf node**), die selbst keine weiteren Knoten enthalten können.

```java
    Text textLeaf = new Text("Hallo");

    StackPane rootNode = new StackPane(textLeaf);

    Scene scene = new Scene(rootNode, 300, 250);
```

### Die Baumstruktur der GUI-Komponenten

Neben dem Bild der Bühne wird für den verschachtelten Aufbau einer GUI oft ein zweites Bild bemüht: das eines Baums mit Wurzel/Stamm, Verzweigungen und Blättern. Das obige Beispiel lässt sich als Baum wie folgt darstellen:

![Die Baumstruktur des Scenegraph](images/Aufbau/01_SceneGraph_Tree.png)

Diese Baumstruktur wird _Scenegraph_ genannt - ein vorwärtsgerichteter Graph mit Knoten (node) und Kanten, bei dem jeder Knoten genau einen Vorgänger hat.

Die Einzelkomponenten hatten wir uns bereits angeschaut. In unserem JavaFX-Beispiel-Quelltext werden Sie vom Blatt (_Leaf_) zur Bühne (_Stage_) erzeugt und zugewiesen. 

```java
@Override
public void start(Stage primaryStage){

    Text textLeaf = new Text("Hallo");

    StackPane rootNode = new StackPane(textLeaf);

    Scene scene = new Scene(rootNode, 300, 250);

    primaryStage.setTitle("Hello World!");
    primaryStage.setScene(scene);
    primaryStage.show();
}
```

### Ausführen des JavaFX-Projekts

Diesmal haben wir am Code selbst keine Änderungen vorgenommen. Wer das Programm trotzdem ausführen will, kann es wie immer direkt aus der IDE aufrufen, oder, indem wir die `pom.xml` mit Maven adressieren:

```bash
mvn javafx:run -f "c:\PATH\TO\PROJECT\jfxhello\pom.xml"
```

### Nächste Schritte

Dieser Artikel ist ein Teil der Artikelserie zu einer Energiemonitors mit [JavaFX](https://oer-informatik.de/javafx-projekt).

Als Nächstes schauen wir uns die zugrunde liegenden Inhaltselemente _branch nodes_ und _leaf nodes_ mal etwas genauer an: [Was sind Widgets, wie baue ich sie ein und wie hinterlege ich Aktionen?](https://oer-informatik.de/jfx_04_widgets_einfuegen)



### Links und weitere Informationen

- [OpenJFX Homepage](https://openjfx.io/)

- [JavaDoc für JavaFX](https://openjfx.io/javadoc/11/index.html)

- [Quelltext der Klasse Application](
https://github.com/openjdk/jfx/blob/master/modules/javafx.graphics/src/main/java/javafx/application/Application.java)
- [Quelltext der Klasse LauncherImpl](https://jar-download.com/artifacts/org.openjfx/javafx-graphics/11/source-code/com/sun/javafx/application/LauncherImpl.java)

