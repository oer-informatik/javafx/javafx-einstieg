## Widgets einfügen

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/jfx_04_widgets_einfuegen</span>

> **tl/dr;** _(ca. 4 min Lesezeit): Wie erstelle ich leaf nodes und branch nodes? Welche habe ich zur Auswahl? Und wie versehe ich Widgets - wie zum Beispiel Buttons mit Events? Artikel ist Teil eines [JavaFX-Tutorials](https://oer-informatik.de/javafx-projekt)._

### Den Scenegraph aufbauen: Layout-Elemente wählen.

Eine neu erstellte _Scene_ erwartet als Parameter einen Wurzelknoten (hier: `rootNode`), der die Basisstruktur der GUI darstellt:

```java
Scene scene = new Scene(rootNode, 300, 250);
```

 Dieser Wurzelknoten muss vom Typ `Parent` erben. Üblicherweise werden in der ersten Ebene Objekte, die von der Klasse `Pane` erben genutzt, da sie als Container die weiteren Inhalte gliedern. Das UML-Klassendiagramm zeigt einen Teil der in Betracht kommenden _branchnodes_.

![Der Aufbau einer JavaFX-GUI](plantuml/JavaFXNodePane.png)

Eine Übersicht der einzelnen Panes gibt es hier:

https://docs.oracle.com/javafx/2/layout/builtin_layouts.htm

### Den Scenegraph befüllen: Control-Elemente wählen.

![Der Aufbau einer JavaFX-GUI](plantuml/JavaFXNodeControl.png)
https://openjfx.io/javadoc/18/javafx.controls/javafx/scene/control/package-summary.html



Die Struktur kann mit _bottom-up_- oder _top-down_-Strategie erzeugt werden. Bottom-Up würden zunächst die Inhaltselemente erzeugt werden (_leaf nodes_), die dann Stück für Stück in einen übergeordneten Knoten eingefügt werden:


```java
TextField eingabeTextField = new TextField("0");
TextField ausgabeTextField = new TextField("0");

Button berechnenButton = new Button("berechnen");    
Button loeschenButton = new Button("löschen");

VBox eingabe = new VBox(eingabeTextField, berechnenButton);
VBox ausgabe = new VBox(ausgabeTextField, loeschenButton);

HBox rootNode = new HBox(eingabe, ausgabe);

Scene scene = new Scene(rootNode, 300, 250);
```

Top-Down würden zunächst die Knoten beginnend mit dem Wurzelknoten erzeugt und am Ende zusammengefügt:

```java
HBox rootNode = new HBox();

VBox eingabe = new VBox();
VBox ausgabe = new VBox();

TextField eingabeTextField = new TextField("0");
TextField ausgabeTextField = new TextField("0");

Button berechnenButton = new Button("berechnen");    
Button loeschenButton = new Button("löschen");

rootNode.getChildren().addAll(eingabe, ausgabe);
ausgabe.getChildren().addAll(ausgabeTextField, loeschenButton);
eingabe.getChildren().addAll(eingabeTextField, berechnenButton);
```

![Der Aufbau einer JavaFX-GUI](images/Rechner/01_rechner.png)


### Controls mit Events versehen

Bislang sind die Buttons noch nutzlos, wir müssen Ereignisse hinterlegen, die passieren, wenn der Button betätigt wird.

![Der Aufbau einer JavaFX-GUI](plantuml/JavaFXNodeControlButton.png)

Eine Methode ist dafür ausschlaggebend: `setOnAction()` (Klasse `ButtonBase`) legt fest, was bei Betätigung des Buttons passiert. Das Ergeignis wird als Objekt erzeugt und im Attribut `onAction` gespeichert.

Die Funktionalität wiederum, die durch einen Knopfdruck ausgelöst wird, wird in der Methode `handle()` in einem Objekt vom Typ `Eventhandler` gespeichert. Wir müssen also mit Hilfe einer Klasse, die das Interface `EventHandler` implementiert die Methode `handle()` implementieren.

Das geht auf einem einfachen und auf einem komplizierten Weg. Der komplizierte deckt auf, wie der einfache Weg funktioniert, daher fangen wir zunächst damit an.

#### Events über Objekte von inneren Klassen

In Java kann ich an jeder Stelle Klassen definierten - auch mitten in einer Methode. Da diese Klasse als Scope (Gültigkeitsbereich) an die umgebende Klasse gebunden sind nennt man diese Klassen _inner classes_. Wir benötigen eine Klasse, die das Interface `EventHandler` und darin die Methode `handle()` implementiert. Unsere Funktionalität ist eine einfache Textausgabe auf dem Terminal:

```java
Button berechnenButton = new Button("berechnen");

class MeineEventklasse implements EventHandler<ActionEvent>{
    public void handle(ActionEvent event){
        System.out.println("Button betätigt");
    }
}

EventHandler<ActionEvent> meinEventHandler = new MeineEventklasse();
berechnenButton.setOnAction(meinEventHandler);
```
Wir instanziieren unsere Klasse (Objekt `meinEventHandler`) und weisen dem Button diesen Event per `setOnAction()` zu. Das klappt schonmal wunderbar, ist aber sehr umständlich...

#### Events über Objekte von anonymen inneren Klassen

Es stellt sich bei der Variante oben die Frage, warum wir die Klasse gesondert erstellen und bezeichnen, wenn wir doch ohnehin nur ein Objekt benötigen? Hierfür weist Java _anonyme innere Klassen_ aus: hier wird die Implementierung der Klasse direkt bei der Objekterzeugung übergeben und ist für kein zweites Objekt verfügbar. Wir kürzen so unser obiges Beispiel etwas ab:

```java
Button berechnenButton = new Button("berechnen");
EventHandler<ActionEvent> meinEventHandler = new EventHandler<ActionEvent>() {
    public  void handle(ActionEvent event){
        System.out.println("Button betätigt");
    }
};

berechnenButton.setOnAction(meinEventHandler);
```

Es wurde also ein Objekt erzeugt und dabei direkt die Methode implementiert. Wichtig sind hierbei: die runden Klammern bei `new EventHandler<ActionEvent>()` und der Semicolon am Ende der Objekterzeugung. Der Code funktioniert 100% genauso wie im ersten Beispiel.

#### Events über Lambda-Ausdrücke

Aber es ist immer noch ganz schön viel Code für eine einfache Ausgabe. Bei Interfaces mit genau einer abstrakten Methode (in Java werden sie "Funktionale Interfaces" genannt) können wir eine Kurzform für anonyme innere Klassen anwenden: die Lambda-Ausdrücke. Wir beschreiben nur, welcher Parameter übergeben wird und welche Implementierung für die Methode genutzt wird.

In unserem Fall:

- Parameter: `event`

- Implementierung: 'System.out.println("Button betätigt");'

Die Schreibweise für einen Lambda-Ausdruck ist: `Parameter->{Implementierung}`

Wir können also abkürzen:

```java
Button berechnenButton = new Button("berechnen");
EventHandler<ActionEvent> meinEventHandler = (event->{System.out.println("Button betätigt");});
berechnenButton.setOnAction(meinEventHandler);
```
Genau genommen weiß Java ja bereits bei der Methode `setOnAction()`, dass wir ein Objekt vom Typ `EventHandler` übergeben wollen. Wir können also noch weiter abkürzen:

```java
Button berechnenButton = new Button("berechnen");
berechnenButton.setOnAction(event->{System.out.println("Button betätigt");});
```

### Die Umrechnung und das Löschen implementieren

Häufig sollen bei ausgelösten Events (wie dem Knopfdruck) nicht einzelne Operationen, sondern ganze Abläufe ausgelöst werden. Diese in Lambda-Ausdrücken zu schreiben kann schnell unübersichtlich werden. Daher rufen wir mit dem Lambda-Ausdruck eine neue Methode auf, die alles weitere übernimmt:

```java
berechnenButton.setOnAction(event->berechne());
loeschenButton.setOnAction(event -> loesche());
```

```java
    public void berechne(){
        double zahl = Double.parseDouble(eingabeTextField.getText());   
        double ergebnnis = ((int) zahl / 2);
        ausgabeTextField.setText(Double.toString(ergebnnis));
    }
```

```java
    public void loesche(){
        eingabeTextField.setText("0");
        ausgabeTextField.setText("0");
    }
```


### Nächste Schritte

Dieser Artikel ist ein Teil der Artikelserie zu einer Energiemonitors mit [JavaFX](https://oer-informatik.de/javafx-projekt).

to be continued...

### Links und weitere Informationen

- [OpenJFX Homepage](https://openjfx.io/)

- [JavaDoc für JavaFX](https://openjfx.io/javadoc/11/index.html)

- [Quelltext der Klasse Application](
https://github.com/openjdk/jfx/blob/master/modules/javafx.graphics/src/main/java/javafx/application/Application.java)
- [Quelltext der Klasse LauncherImpl](https://jar-download.com/artifacts/org.openjfx/javafx-graphics/11/source-code/com/sun/javafx/application/LauncherImpl.java)

