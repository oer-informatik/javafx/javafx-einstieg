## Ein erstes "Hello World" von JavaFX mit Maven

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/jfx_01_initialisierung_des_projekts</span>

> **tl/dr;** _(ca. 6 min Lesezeit): Ein JavaFX-Projekt wird aus einer Vorlage (Maven Artefakt) erstellt und Konfiguriert. Die Konfigurationsdatei `pom.xml` wrid für JavaFX und JDK17 angepasst, die Startklasse erstellt und die JavaFX-App per IDE (VSCode) und Konsole mit Maven gestartet. Artikel ist Teil eines [JavaFX-Tutorials](https://oer-informatik.de/javafx-projekt)._

### Das Build-System Maven

Software-Projekte nutzen häufig Bibliotheken oder Module, die geladen und eingebunden werden müssen. Beispielsweise wollten wir das Modul JavaFX nutzen, haben dies aber bislang noch gar nicht installiert. Wir können uns jetzt selbst um alle Abhängigkeiten kümmern, diese aus dem Netz laden, einbinden (in einer `module-info.java`- Datei) usw. oder uns von einem Build-System wie _Maven_ oder _Gradle_ helfen lassen.

In diesem Beispiel werde ich dafür _Maven_ nutzen, dass uns im gesamten Build-Prozess mit Automatisierungen unterstützen wird. (Wer mehr dazu erfahren will, kann z.B. [diesen Artikel zum Buildprozess mit Maven](https://oer-informatik.de/build-tool-maven) lesen.)

### Ein neues Maven/Java(FX)-Projekt erzeugen

Um ein JavaFX-Maven Projekt zu erstellen benötigen wir zunächst zwei Dinge: eine Ordnerstruktur, wie Maven sie nutzt und eine Konfigurationsdatei `pom.xml`.

![Maven-Dateistruktur](https://oer-informatik.gitlab.io/java-advanced/build-tool-maven/plantuml/mvn-ordnerstruktur.png)

Beides lassen wir uns aus einer Vorlage (Maven Quickstart) erstellen und passen es dann an unsere Bedürfnisse an. Wir können uns von VSCode helfen lassen (nächster Absatz) oder einfach per Einzeiler ein Maven-Projekt in der Kommandozeile (Bash/Powershell/Terminal) erstellen:

```bash
mvn -B archetype:generate -D groupId=de.csbme.ifaxx.myfxproject -D artifactId=jfxhello -D archetypeArtifactId=maven-archetype-quickstart -D archetypeVersion=1.4
```

Wem der Weg über die Kommandozeile zu nerdy ist, der kann in VSCode über das installierte _Java Extension Pack_ das folgende Kommando ausführen. Per Tastatur: `Strg-Shift-P` oder über das Menü _Help / Show All Commands_:

![Menü Help / Show All Commands](images/VSCode/02_Help_Commands.png)

In dem Eingabefeld kann per Dropdown eingegeben oder per Eingabe vorausgewählt werden. Bei Eingabe von "Create Java" erscheint der für uns richtige Punkt "Java: Create Java Project":

![Eingabe "Create Java" sollte das Auswahlmenü "Create"](images/VSCode/02_CreateJavaProject.png)

Wir lassen uns ein Standard Maven-Projekt erzeugen, daher wähle ich "Maven Create from Archetype".

![Eingabe "Maven Create from Archetype"](images/VSCode/03_CreateMavenFromArchetype.png)

Jetzt müssen wir die Vorlage wählen: "Maven Archetype Quickstart" bietet alles, was wir brauchen.

![Eingabe "Maven Archetype Quickstart"](images/VSCode/03a_CreateMavenArchetypeQuickstart.png)

Bei der folgenden Versionsabfrage wählen wir die aktuellste (momentan "1.4").

Die GruppenID ist so etwas, wie eine umgekehrte Domäne: sie dient dazu, Pakete und Module zu gliedern und zu finden (kleingeschrieben). Ich wähle immer die Struktur (Land).(Organisation).(Abteilung).(Projekt), also in unserem Fall `de.csbme.ifaxx.myfxproject`:

![GruppenID eingeben: "de.csbme.ifaxx.myfxproject"](images/VSCode/04_GroupID.png)

Als Name des Softwareprodukts ("Artefakt") wähle ich "jfxhello" (kleingeschrieben) für ein erstes "Hello World":

![Name des Softwareprodukts eingeben: "jfxhello"](images/VSCode/05_ArtifactID.png)

Anschließend muss noch der Speicherort gewählt werden.

Wenn das geschehen ist, geht es im Terminal (unten in VS Code) weiter: Maven beginnt direkt damit, die nötigen Abhängigkeiten zu laden und sich rückzuversichern, dass die erste Programmversion `1.0-SNAPSHOT` heißen soll (was Sinn ergibt und mit der `Enter`-Taste bestätigt werden kann). Anschließend müssen alle Eingaben noch einmal bestätigt werden (mit "Y" oder Return): 

![Eingaben mit Enter und "Y" bestätigen](images/VSCode/08_ConfirmProperties.png)

Jetzt wird die Zusammenfassung ausgegeben, die Dateien werden unter dem Pfad angelegt und man hat die Möglichkeit, den Ordner direkt zu öffnen ("Open" unten in der rechten Ecke):

![Eingaben mit "Y" oder Return bestätigen](images/VSCode/09_Success.png)

### Was wurde erzeugt?

Das Buildsystem Maven hat eine Verzeichnisstruktur, Konfigurationsdateien und ein kleines Beispielprogramm mit Test angelegt. Wir wollen uns zunächst zwei Dateien anschauen: die `pom.xml` und die Startdatei der App, die darin konfiguriert wird:

![Inhalt der pom.xml](images/VSCode/10_pomxml.png)

In der `pom.xml` werden die Eingaben, die wir im Rahmen der Projektinitialisierung gemacht haben, gespeichert (gelb markiert). Wir können diese dort ändern - und das müssen wir gleich auch, denn einige genannte Abhängigkeiten sind veraltet (rot gekennzeichnet), andere fehlen komplett. Am einfachsten ist es, die `pom.xml` zu ersetzen mit dem Quelltext unten - mindestens ab dem Punkt `<properties>...`. 

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>de.csbme.ifaxx.myfxproject</groupId>
	<artifactId>jfxhello</artifactId>
	<version>1.0-SNAPSHOT</version>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<main.class>de.csbme.ifaxx.myfxproject.App</main.class>
		<java.version>17</java.version>
		<maven.compiler.source>${java.version}</maven.compiler.source>
		<maven.compiler.target>${java.version}</maven.compiler.target>
		<maven.compiler.release>${java.version}</maven.compiler.release>
	</properties>

	<description>An example JavaFX ${java.version} project</description>

	<dependencies>
		<dependency>
			<groupId>org.openjfx</groupId>
			<artifactId>javafx-controls</artifactId>
			<version>21.0.1</version>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter</artifactId>
			<version>5.10.1</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.openjfx</groupId>
				<artifactId>javafx-maven-plugin</artifactId>
				<version>0.0.8</version>
				<configuration>
					<mainClass>${main.class}</mainClass>
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>
```

Mit diesem Aufbau wird ein Projekt mit Java Version 17 gebaut (`<java.version>17</java.version>`), dessen Startklasse `src/main/java/de/csbme/ifaxx/myfxproject/App.java` ist (`<mainClass>de.csbme.ifaxx.App</mainClass>`). Die nötigen Maven-Compiler-Versionen und aktuellen Anhängigkeiten (`openjfx`, `junit.jupiter`) sind ebenso gegeben. Wichtig ist, dass in den Properties die Variablen für `java.version` und `main.class`
gesetzt werden, da diese unten referenziert werden.

### Anpassen der Java-Dateien

In der Vorlage wurden noch zwei Java-Dateien angelegt: `App.java` und `AppTest.java`. Das eigentliche Hauptprogramm `App.java` ist derzeit noch nicht Java-FX-spezifisch und die Testklasse `AppTest.java` verweist auf eine veraltete Version. Wir passen beide zunächst mit einer einfachen Struktur an, um auszuprobieren, ob das ganze schon lauffähig ist. Im folgenden Artikel werden wir dann den Aufbau und die Funktion genauer betrachten.

Der Startpunkt unserer App ist die Datei `src/main/java/de/csbme/ifaxx/myfxproject/App.java`. Ohne auf die Inhalte eingehen zu wollen: wir benötigen Imports, müssen einen Vererbungshinweis auf `Application` ergänzen, benötigen eine `start()`-Methode und müssen in der `main()`-Methode `launch(args);` ergänzen. Im Ganzen muss sie für einen ersten Startversuch den folgenden Inhalt haben:

```java
package de.csbme.ifaxx.myfxproject;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class App extends Application {
    @Override
    public void start(Stage primaryStage){
        
        primaryStage.setScene(new Scene( new StackPane(new Text("Hallo")), 300, 250));
        primaryStage.show();
}

    public static void main(String[] args) {
        launch(args);
    }
}
```

In der Testklasse  `src/test/java/de/csbme/ifaxx/myfxproject/AppTest.java` können wir ordentlich aufräumen: die Imports auf jUnit sind veraltet, am einfachsten wir löschen erstmal alles, bis auf die Klasse selbst:

```java
package de.csbme.ifaxx.myfxproject;

public class AppTest {

}
```

Damit sollte die Struktur für einen ersten Test vorhanden sein.

### Ausführen des JavaFX-Projekts

Wenn die IDE alles korrekt erkannt hat, sollte links im Menü der Punkt "Maven" auftauchen, der die Unterpunkte "Lifecyle" und "Plugins" hat. Unter Plugins findet sich "javafx" und darin "run". Damit lässt sich das Programm ausführen.

![Inhalt der pom.xml](images/VSCode/11_run.png)

Falls das nicht klappt, bleibt der Weg über das Terminal (PowerShell/Bash): hier muss Maven der Pfad zur `pom.xml`-Datei übergeben werden:

```bash
mvn javafx:run -f "c:\PATH\TO\PROJECT\jfxhello\pom.xml"
```

In beiden Fällen sollte eine recht nüchterne erste GUI "Hallo" sagen:

![Das erste JavaFX-Fenster gibt "Hello" aus](images/VSCode/13_helloworld.png)

Damit haben wir schon einmal eine Reaktion des Systems ohne uns im Ansatz mit den Hintergründen beschäftigt zu haben. Im nächsten Schritt untersuchen wir den [Aufbau dieser "Hello World" App mit JavaFX](https://oer-informatik.de/jfx_02_helloworld_und_aufbau).

 ### Mögliche Fehlermeldungen

Bis hierhin können viele Probleme auftreten, die eigentlich immer dieselbe Ursache haben: Die Java-Version in der `pom.xml`, im `PATH`, unter `JAVA_HOME`, Maven oder der IDE passen nicht zusammen. Dann heißt es: nochmal einen Schritt zurück gehen und die Umgebungsvariablen korrekt einstellen oder den Aufruf zunächst über die PowerShell/Bash versuchen.

 ```bash
 Fatal error compiling: error: invalid target release: 21
```

### Nächste Schritte

Dieser Artikel ist ein Teil der Artikelserie zu einer Energiemonitors mit [JavaFX](https://oer-informatik.de/javafx-projekt).

Das Projekt ist erstellt, jetzt kommt das unweigerliche [Hello World mit JavaFX](https://oer-informatik.de/jfx_02_helloworld_und_aufbau).

### Links und weitere Informationen

- [OpenJFX Homepage](https://openjfx.io/)

- [JavaDoc für JavaFX](https://openjfx.io/javadoc/11/index.htmlhttps://openjfx.io/javadoc/17/index.html)
