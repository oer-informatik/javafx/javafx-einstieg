package de.csbme.ifaxx.myfxproject;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class App extends Application {
    @Override
    public void start(Stage primaryStage){

        Text textLeaf = new Text("Hallo");

        StackPane rootNode = new StackPane(textLeaf);

        Scene scene = new Scene(rootNode, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
}

    public static void main(String[] args) {
        launch(args);
    }
}
