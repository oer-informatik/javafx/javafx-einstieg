package de.csbme.ifaxx.myfxproject;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class AppTest {


    @Test
    public void testExample() {
        //given
        String expected = "Replace with expected result of SUT-Method";

        //when
        String result = "Replace by SUT-Method invocation to get result";

        //then
        assertEquals(expected, result);
    }
}
