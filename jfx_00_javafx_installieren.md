## Das JavaFX-Framework installieren und die IDE konfigurieren

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/jfx_00_javafx_installieren</span>

> **tl/dr;** _(ca. 2 min Lesezeit): Grundvoraussetzung ist die Installation einer aktuellen IDE (VSCode, ein einfacher Editor reicht auch), des JavaJDK (ab Version 17) und ein Buildtool (ich nutze in diesem Tutorial Maven). Der spannende Teil ist, dafür zu sorgen, dass das System auch die gewünschten Programme findet - also Java JDK und Maven in den Umgebungsvariablen einzutragen. Das wird per Terminal/Shell überprüft. Der Artikel ist Teil eines [JavaFX-Tutorials](https://oer-informatik.de/javafx-projekt)._


### Installationen und Voraussetzungen

Um ein JavaFX-Projekt bauen benötigen das _Java Development Kit_, ein Buildtool (ich nutze in diesem HowTo _Maven_) und eine Entwicklungsumgebung. Konkret werden folgende Versionen vorgeschlagen:

#### Eine aktuelle Java Entwicklungsumgebung

Java-Programme werden in einer Laufzeitumgebung ausgeführt - der _Java Virtual Machine_. Diese ist i. d. R. auf Betriebssystemen in einer halbwegs aktuellen Version vorhanden. Das Paket nennt sich _Java Runtime Engine_ (_JRE_) und stellt den Interpreter bereit, der den Bytecode, in dem Java-Programme ausgeliefert werden (`*.jar`, `*.class`) ausführen kann.

Um Java programmieren zu können benötigen wir eine Umgebung, die diesen Bytecode aus Quelltext erzeugt, den Java Compiler. Dieser wird im _Java Development Kit_ (_JDK_) bereitgestellt. Da Java unter der Federführung von Oracle als _Community_-Projekt organisiert wird, gibt es verschiedene Anbieter eines _JDK_. Für uns zunächst am wichtigsten: Damit wir die aktuellste Version von JavaFX nutzen können, benötigen wir **mindestens die JDK Version 17 (oder neuer)**.

Oracle veröffentlicht [alle halbe Jahre eine neue Version](https://www.oracle.com/java/technologies/java-se-support-roadmap.html) des _Java Development Kit_ (JDK), für die es jeweils ein halbes Jahr Updates gibt. Um nicht in schnellen halbjährlichen Taktung aktualisieren zu müssen, empfehle ich, diese JDK-Versionen nicht zu verwenden. Sie sind v.a. relevant, wenn man immer die neusten Features benötigt - das tun wir hier nicht. Eine solide und aktuelle Basis stellt die jeweils jüngste Version mit _Long Term Support_ dar. Derzeit ist die Version 21 (mit Updates bis 2028) aktuell, die im September 2023 erschienen ist. Zum Zeitpunkt dieses Artikels wird sie jedoch noch nicht von allen IDE komplett unterstützt. Einige Schritte können daher noch nicht mit Unterstützung der IDE, sondern nur händisch über die Konsole ausgeführt werden. Für weniger versierte Programmierer*innen empfiehlt sich daher die Version 17 LTS (Support bis 2026), deren Funktionsumfang komplett ausreicht.

-  Direkt von Oracle gibt es die _Java JDK LTS_. Oracle bietet diese Version jedoch nicht unter freier Lizenz an, ggf. ergeben sich dadurch für die Benutzung außerhalb des Bildungsbereichs und auf ArbeitsPCs Einschränkungen. Download: https://www.oracle.com/java/technologies/downloads/#java21

- OpenJDK LTS von anderen Herstellern: Da Oracle für die OpenSource-(GPLv2)-Variante der OpenJDK keinen Langzeitsupport anbieten, haben [eine Reihe anderer Hersteller](https://de.wikipedia.org/wiki/OpenJDK#OpenJDK-Varianten) das übernommen. Es gibt beispielsweise OpenJDKs mit LTS von [Adoptikum](https://adoptium.net/de/) oder [RedHat derzeit nur JDK 17 LTS](https://access.redhat.com/documentation/en-us/red_hat_build_of_openjdk/17). Für alle, die sich gegen die "closed Source"-LTS-Variante von Oracle entscheiden, eine gute Alternative.

- [OpenJDK von Oracle (GPLv2)](http://jdk.java.net/): Wer immer die neuste Java-Version haben will und bereit ist, halbjährlich zu aktualisieren, für den stellt Oracle eine offene (unter GPLv2 nutzbare) Version bereit.

Nach Download und Installation (Anleitungen siehe Links der Hersteller) kommt der eigentlich wichtige Teil: die Umgebungsvariablen müssen unbedingt gesetzt werden:

- eine Variable `JAVA_HOME` muss das aktuelle JDK-Verzeichnis referenziert werden

- im `PATH` das darunterliegende `/bin`-Verzeichnis, in dem sich der Compiler befindet (`javac`). 

Für Windows: Einstellungen / Systemumgebungsvariablen bearbeiten / Umgebungsvariablen / Eintragen des Java-Pfads als `JAVA_HOME` Variable, Anhängen von `JAVA_HOME/bin` in die Variable `PATH`. Details und Infos für andere OS finden sich in der Anleitung unter: [https://www.java.com/de/download/help/path.xml]().

Hat die Installation geklappt und ist die Java JDK im `PATH`? Überprüfen wir es in der Konsole:

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="shell" data-tabid="'bash" onclick="openTabsByDataAttr('bash', 'shell')">Bash/\*nix</button>
   <button class="tablink" data-tabgroup="shell" data-tabid="powershell" onclick="openTabsByDataAttr('powershell', 'shell')">PowerShell/Windows</button>
   <button class="tablink" data-tabgroup="shell" data-tabid="all" onclick="openTabsByDataAttr('all', 'shell')">_all_</button>
    <button class="tablink" data-tabgroup="shell" data-tabid="none" onclick="openTabsByDataAttr('none', 'shell')">_none_</button>
</span>

<span class="tabs" data-tabgroup="shell" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="bash">

Unter Debian/Linux:


Welche Version wurde installiert und hat die Installation geklappt (im Terminal bzw. der PowerShell)?

```bash
$ java -version
```
Die Ausgabe sollte etwa sein:

```
java version "21.0.1" 2023-10-17 LTS
Java(TM) SE Runtime Environment (build 21.0.1+12-LTS-29)
Java HotSpot(TM) 64-Bit Server VM (build 21.0.1+12-LTS-29, mixed mode, sharing)
```

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="powershell"  style="display:none">

```powershell
PS C:\> java -version
```

Die Ausgabe sollte etwa sein:

```
java version "21.0.1" 2023-10-17 LTS
Java(TM) SE Runtime Environment (build 21.0.1+12-LTS-29)
Java HotSpot(TM) 64-Bit Server VM (build 21.0.1+12-LTS-29, mixed mode, sharing)
```

Manche Programme greifen über die Umgebungsvariable `JAVA_HOME` auf Java zu. Wie ist diese gesetzt?

```powershell
PS C:\> Write-Output $Env:JAVA_HOME 
```

```
C:\Program Files\Java\jdk-17.0.4
```

In meinem Fall zeigt sie noch auf die ältere Version, obwohl bereits die LTS21 installiert ist und auch bei `java -v` korrekt ausgegeben wird. Eine klassische Fehlerquelle, das sollte geändert werden (siehe link oben zu Systemvariablen ändern).

Welche Java-Versionen stehen im PATH? Die PowerShell findet so alle Pfade, die Java enthalten:

```powershell
Write-Output $Env:Path | Out-String -Stream | ForEach-Object { $_.Split(";") } | Select-String -Pattern "java" -SimpleMatch
```

Das Ergebnis kann auch eine längere Liste sein:

```
C:\Program Files\Common Files\Oracle\Java\javapath
C:\Program Files (x86)\Common Files\Oracle\Java\javapath
C:\Program Files\Java\jdk-11.0.4\bin
C:\Program Files\NetBeans-11.1\netbeans\java\maven\bin
C:\Program Files\Java\jdk-17.0.4
```

Wenn `java -v` kein Ergebnis liefert - oder die falsche Version - dann sollte der `PATH` mal überprüft werden!

</span>

(Sofern hier noch `java version "1.8.0_261"` oder eine Version \<17.0 gemeldet werden sollte, muss auf jeden Fall überprüft werden, ob ein aktuelles Java installiert ist und dies ggf. nachgeholt werden! Diese Version ist definitiv zu alt!)

#### Eine JavaFX-fähige IDE, z.B. VS Codium

Es gibt mehrere Entwicklungsumgebungen (IDE), die eine gute JavaFX-Unterstützung umsetzen, unter [openjfx.io/openjfx-docs](https://openjfx.io/openjfx-docs/) finden sich aktuelle Anleitungen zu den wichtigsten Entwicklungsumgebungen:

- Visual Studio Code (bzw. besser: die Opensource-Variante VS Codium) mit dem _Java Extension Pack_  - Download z.B. hier [VSCodium - für Win z.B. VSCodium-win32-x64-VERSION.zip ](https://github.com/VSCodium/vscodium/releases)

- [Eclipse](https://www.eclipse.org/efxclipse/index.html)

- IntelliJ IDEA [JavaFX ist bereits in der _community edition_ enthalten, Student*innen und Schüler*innen erhalten gegen Nachweis die Ultimate-Version](https://www.jetbrains.com/help/idea/javafx.html)

- [Netbeans](https://netbeans.apache.org/download/),

In den folgenden Beispielen nutze ich VS Codium.

##### Die nötigen Extensions installieren (VS Codium)

Theoretisch langt ein normaler Editor und auch VS-Code ließe sich als solcher benutzen. Die volle Java-Funktionalität erhält man jedoch erst mit ein paar Erweiterungen - zu aller erst mit dem "Java Extension Pack":

![Unter "Extensions" Java Extension Pack wählen](images/VSCode/01_InstallJavaExtensionPack.png)


Darüber hinaus lassen sich Projekte mit Maven leichter verwalten, wenn "Maven for Java" installiert ist:

![Unter "Extensions" "Maven vor Java" wählen (gleiches Symbol wie Java Extension Pack)](images/VSCode/01_InstallMavenforJavaExtension.png)


#### Build-Tool installieren (Maven / Gradle)

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="buildTool" data-tabid="maven" onclick="openTabsByDataAttr('maven', 'buildTool')">Maven</button>
   <button class="tablink" data-tabgroup="buildTool" data-tabid="gradle" onclick="openTabsByDataAttr('gradle', 'buildTool')">Gradle</button>
   <button class="tablink" data-tabgroup="buildTool" data-tabid="all" onclick="openTabsByDataAttr('all', 'buildTool')">_all_</button>
    <button class="tablink" data-tabgroup="buildTool" data-tabid="none" onclick="openTabsByDataAttr('none', 'buildTool')">_none_</button>
</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="maven">

Ich nutze für die Beispiele das Buildtool Maven. Maven sollte installiert und direkt im `PATH` gefunden wird (das kann im Terminal/PowerShell mit `mvn -v` geprüft werden).

Die Maven-Binaries lass sich auf [dieser Seite](https://maven.apache.org/download.cgi) laden, im passenden Pfad für Programme entpacken und entsprechend im `PATH` eingetragen. (Windows Einstellungen Systemumgebungsvariablen bearbeiten / Umgebungsvariablen / Eintragen des Pfads in `PATH`, Details und Infos für andere OS [siehe z.B. hier](https://maven.apache.org/install.html)).

Nach einem Neustart des Terminals bzw. der IDE sollte Maven gefunden werden:

```bash
mvn -v
```

Sollte etwa folgendes zurückgeben:

```
Apache Maven 3.8.6 (84538c9988a25aec085021c365c560670ad80f63)
Maven home: C:\Program Files\apache-maven-bin\apache-maven-3.8.6
Java version: 11.0.4, vendor: Oracle Corporation, runtime: C:\Program Files\Java\jdk-11.0.4
Default locale: de_DE, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```

Wenn Maven nicht gefunden wird, kann man über die PowerShell nachschauen, ob es im `PATH` steht:

```powershell
Write-Output $Env:Path | Out-String -Stream | ForEach-Object { $_.Split(";") } | Select-String -Pattern "maven" -SimpleMatch
```

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="gradle"  style="display:none">

Seit der letzten Version nutzt beispielsweise der SpringInitializr per Default das Buildtool Gradle. In der Regel ist es in den modernen IDEs direkt enthalten - es kommt aber vor, dass Projekte direkt über die Konsole ausgeführt werden sollen. Dazu ist es erforderlich, dass eine Gradle Version direkt im `PATH` gefunden wird (das kann im Terminal/PowerShell mit `gradle -v` geprüft werden).

Die Gradle-Binaries lass sich auf [dieser Seite](https://docs.gradle.org/current/userguide/installation.html) laden, im passenden Pfad für Programme entpacken (Win: `C:/Gradle`) und entsprechend im `PATH` eingetragen. (Windows Einstellungen Systemumgebungsvariablen bearbeiten / Umgebungsvariablen / Eintragen des Pfads in PATH, Details [siehe z.B. hier (nach "PATH" suchen und eigenes Betriebssystem wählen)](https://docs.gradle.org/current/userguide/installation.html)).

Nach einem Neustart des Terminals bzw. der IDE sollte Gradle gefunden werden:

```bash
gradle -v
```

Sollte etwa folgendes zurückgeben:

```
Welcome to Gradle 8.3!

Here are the highlights of this release:
 - Faster Java compilation
 - Reduced memory usage
 - Support for running on Java 20

For more details see https://docs.gradle.org/8.3/release-notes.html


------------------------------------------------------------
Gradle 8.3
------------------------------------------------------------

Build time:   2023-08-17 07:06:47 UTC
Revision:     8afbf24b469158b714b36e84c6f4d4976c86fcd5

Kotlin:       1.9.0
Groovy:       3.0.17
Ant:          Apache Ant(TM) version 1.10.13 compiled on January 4 2023
JVM:          17.0.4 (Oracle Corporation 17.0.4+11-LTS-179)
OS:           Windows 10 10.0 amd64
```

Wenn Gradle nicht gefunden wird, kann man über die PowerShell nachschauen, ob es im `PATH` steht:

```powershell
Write-Output $Env:Path | Out-String -Stream | ForEach-Object { $_.Split(";") } | Select-String -Pattern "gradle" -SimpleMatch
```

</span>


### Nächste Schritte

Bisher haben wir nur ein paar Installationen aufgespielt, im nächsten Schritt können wir jetzt die [Initialisierung der Projekts vornehmen](https://oer-informatik.de/jfx_01_initialisierung_des_projekts) und ein erstes "Hello World" ausgeben.

Dieser Artikel ist ein Teil der Artikelserie zu einer [JavaFX-Decarbonizer-App](https://oer-informatik.de/javafx-projekt).

### Links und weitere Informationen

- [OpenJFX Homepage](https://openjfx.io/)

